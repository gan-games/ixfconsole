var com = com || {};
com.igt = com.igt || {};
com.igt.console = com.igt.console || {};

com.igt.console.slider = function (bridge,elSlider,elActivate,elDeactivate,orientationChange) {

    var EventTarget = function() {
        this.listeners = {};
    };
      
    EventTarget.prototype.listeners = null;
    EventTarget.prototype.addEventListener = function(type, callback) {
        if (!(type in this.listeners)) {
            this.listeners[type] = [];
        }
        this.listeners[type].push(callback);
    };
    
    EventTarget.prototype.removeEventListener = function(type, callback) {
        if (!(type in this.listeners)) {
            return;
        }
        var stack = this.listeners[type];
        for (var i = 0, l = stack.length; i < l; i++) {
            if (stack[i] === callback){
            stack.splice(i, 1);
            return;
            }
        }
        };
        
        EventTarget.prototype.dispatchEvent = function(event) {
        if (!(event.type in this.listeners)) {
            return true;
        }
        var stack = this.listeners[event.type].slice();
        
        for (var i = 0, l = stack.length; i < l; i++) {
            stack[i].call(this, event);
        }
        return !event.defaultPrevented;
    };

    var controlTower = new EventTarget();
    var gameStarted = 0;
    var enabled = 0; //The console can be activated by the player
    var active = 0;  //The console is currently open
    var activating = 0; //set when the consol is in the process of actiavting
    var deactivating = 0; //set when the console is in the process of deactivating
    var visiblePopup = 0; //if a popup is currently visible, set to it's root node
    var content = document.getElementById("content");
    var messageCode;
    var wagerAborted = false;
    
    elSlider.addEventListener('webkitTransitionEnd', function () {
        //just becoming active
        if (deactivating) {
            elActivate.style.display = '';
            bridge.console.activate("35px");
            deactivating = 0;
            bridge.console.resume();
        }else {
            elDeactivate.style.display = '';
        }
        elSlider.style.webkitTransition = '';
    });

    //@todo the show and hide methods should be overridable
    function showConsole() {

        if (window.console)console.log("showconsole. enabled: " + enabled + "  active: " + active + "  activating: " + activating);

        if (enabled && !active && !activating) {
            activating = 1;
            elActivate.style.display = 'none';
            elDeactivate.style.display = 'none';
            bridge.console.activate(elSlider.offsetHeight + 'px');
            active = !active;
        }
    };

    function hideConsole() {

        if (window.console)console.log("hideconsole");

        if (active && !deactivating) {
            elDeactivate.style.display = 'none';
            deactivating = 1;
            elSlider.style.webkitTransition = 'top .2s linear';
            elSlider.style.top = -content.offsetHeight + 'px'; //slide up
            active = !active;
            //remove focus so the keyboard disappears. Doesn't work though :(
            document.activeElement && document.activeElement.blur();
        }
    }

    function enableConsole(enable) {
        if (window.console)console.log("enableConsole");
        enabled = enable;
        // elActivate.className = enabled ? 'enabled' : 'disabled';
    }

    function updateLayout() {
        if (visiblePopup) {
            bridge.console.activate(visiblePopup.offsetHeight + 'px');
        } else if (enabled && active) {
            slider.style.display = '';
            bridge.console.activate(slider.offsetHeight + 'px');
        } else if (gameStarted) {
            //there si a minor bug here after an orientation change
            slider.style.display = '';
            slider.style.top = -content.offsetHeight + 'px';
            bridge.console.activate("35px");
        } else {
            bridge.console.activate(0);
        }
        orientationChange();
    };

    function showPopup(popup) {
        visiblePopup && hidePopup();
        visiblePopup = popup;
        visiblePopup.style.display = 'block';
        elSlider.style.display = 'none';
        bridge.console.activate(visiblePopup.offsetHeight + 'px');
    };

    function hidePopup() {
        if (visiblePopup) {
            document.activeElement && document.activeElement.blur();
            visiblePopup.style.display = '';
            visiblePopup = 0;
            updateLayout();
        }
    };

    function isEnabled () {
        return Boolean(enabled);
    }

    elActivate.addEventListener(document.body.ontouchstart ? 'touchstart' : 'mousedown', showConsole);
    elDeactivate.addEventListener(document.body.ontouchstart ? 'touchstart' : 'mousedown', hideConsole);

    function continueAfterRC() {
        console.log("slider.js continueAfterRC");

        hidePopup();
        hideConsole();
        enableConsole(0);
        //need to test whether to wait for the console to minimise
        if (!deactivating) {
            bridge.console.resume();
        }
    }

    window.continueAfterRC = continueAfterRC.bind(this);

    function ganRealityCheckResponse(isTriggered, timePlaying, interval) {
        console.log("slider.js ganRealityCheckResponse invoked");

        if (isTriggered) {
            realityCheckPopup(timePlaying);
        } else {
            continueAfterRC();
        }
    };

    window.ganRealityCheckResponse = ganRealityCheckResponse.bind(this);

    function realityCheckPopup(timePlaying) {

        var messageSurround = createAlert(Locale.getMsg("messages.gRealityCheck", [timePlaying]));

        var button = document.createElement('button');
        button.textContent = Locale.getMsg("actions.gContinueCaps");
        button.addEventListener('click', function () {
            messageSurround.parentElement.removeChild(messageSurround);
            // continueAfterRC();
            ganContinueAfterCheck();
            bridge.console.resume();
        })
        button.className = 'btn';
        document.getElementById('alert_box').appendChild(button);

        var button = document.createElement('button');
        button.textContent = Locale.getMsg("monetary.gviewHistory");
        button.addEventListener('click', function () {
            window.open("/account-history.shtml", "accountWagers");
        })
        button.className = 'btn';
        document.getElementById('alert_box').appendChild(button);

        var button = document.createElement('button');
        button.textContent = Locale.getMsg("html5.realityCheck");
        button.addEventListener('click', function () {
            window.open("/realityCheck.do", "responsibleGambling");
        })
        button.className = 'btn';
        document.getElementById('alert_box').appendChild(button);

        var button = document.createElement('button');
        button.textContent = Locale.getMsg("monetary.gcashout");
        button.addEventListener('click', function () {
            closeGame();
        })
        button.className = 'btn';
        document.getElementById('alert_box').appendChild(button);

        _slider.showPopup(messageSurround);
    }

    function addEventListener( type, listener ) {
        controlTower.addEventListener( type, listener );
    }

    function dispatchEvent( type ) {
        controlTower.dispatchEvent( new CustomEvent( type, { detail: Array.prototype.slice.call( arguments ) } ) );
    }

    bridge.addEvents({
        consoleResized: function (_height) {
            if (activating) {
                slider.style.webkitTransition = 'top .2s linear';
                slider.style.top = 0; //slide down
                activating = 0;
            }
            dispatchEvent( 'consoleResized' );
        },
        gameReady: function () {
            gameStarted = 1;
            enableConsole(1);
            dispatchEvent( 'gameReady' );
            updateLayout();
        },
        wagerIsStarting: function () {

            if (!window.gan) {
                continueAfterRC();
                return;
            }

            if (!!gan.compinator && window.realityCheck === 1) {
                gan.compinator.performRealityCheck();
                return;
            }
            dispatchEvent( 'wagerIsStarting' );
            continueAfterRC();
        },
        gameOutcome: function () {
            enableConsole(1);
            bridge.console.resume();

            if ( messageCode ) {
                dispatchEvent( "closeFromIGTPopup", messageCode )
                messageCode = null;
            }
            wagerAborted = false;
        },
        wagerIsComplete: function () {
            enableConsole(1);
            dispatchEvent( 'wagerIsComplete' );
            bridge.console.resume();
        },
        wagerIsAborted: function () {
            enableConsole(1);
            dispatchEvent( 'wagerIsAborted' );
            wagerAborted = true;
            bridge.console.resume();
        },
        orientationchange: updateLayout,
        insufficientFundsNotification: function(){
            //@todo I think this should be happening after the QTO
            enableConsole(1);
            bridge.console.resume();
        },
        displayMessage: function (code) {
            messageCode = code;
            bridge.console.resume();
        }
    });

    return {
        showConsole: showConsole,
        hideConsole: hideConsole,
        enableConsole: enableConsole,
        updateLayout: updateLayout,
        showPopup: showPopup,
        hidePopup: hidePopup,
        isEnabled: isEnabled,
        addEventListener: addEventListener,
        dispatchEvent: dispatchEvent
    };
};